function createNewUser() {
  let firstName = prompt("Введіть ім'я:");
  let lastName = prompt("Введіть прізвище:");
  let birthday = prompt("Введіть дату народження (у форматі dd.mm.yyyy):");

  const newUser = {
    firstName,
    lastName,
    birthday,
    getAge() {
      const today = new Date();
      const birthDate = new Date(this.birthday);

      let age = today.getFullYear() - birthDate.getFullYear();
      const monthDiff = today.getMonth() - birthDate.getMonth();

      if (
        monthDiff < 0 ||
        (monthDiff === 0 && today.getDate() < birthDate.getDate())
      ) {
        age--;
      }

      return age;
    },
    getPassword() {
      const password =
        this.firstName.charAt(0).toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(-4);
      return password;
    },
  };

  return newUser;
}

const user = createNewUser();
console.log(user);
console.log("Вік користувача:", user.getAge());
console.log("Пароль користувача:", user.getPassword());

// 1. Екранування дозволяє використовувати спеціальні символи в якості звичайних. Це необхідно, наприклад,
// для пошуку символу в string, який поза його межами використовується як спецсимвол, або для відображення в html
// спецсимволів як звичайних символів.. Воно дозволяє вставляти спеціальні символи, контрольні послідовності,
// юнікод-символи та інші спеціальні значення всередину рядків без порушення правильності програми.
//  2. A. function declaration:

//  function func(params) {
//  //body
//  return;
//  }

//  B. function expression:

//  const func = function(params) {
//      //body
//      return;
//  }

//  C. Arrow function:

//  (params) => { //body }

//  D. Function constructor

//  const func = new Function();

//  3. Hositing це фактично застосованість певної частини коду залежно від того, в якому фрагменті коду
//  ця частина написана. Простими словами, здатність посилатися на функцію або змінну до того,
//  як виконаються попередні фрагменти.
//  Для функцій hoisting працює залежно від способу створення. Function declaration хойститься, інші способи створення - ні.
//  Для змінних hoisting теж працює, однак викликати змінні conts & let не можна (не через hoisting, а через те, що
//  до моменту, поки код не пройде до оголошення змінної, її не буде ініціалізовано). Змінну var можна викликати з будь-якої
//  частини коду незалежно від того, де вона оголошена, оскільки вона є глобальною.
//  Деякі джерела пояснюють hoisting як умовне "перенесення" функцій та змінних догори коду.
